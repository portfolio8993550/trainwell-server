require('dotenv').config()
const { Configuration, OpenAIApi } = require('openai');

const configuration = new Configuration({
    apiKey: process.env.OPEN_AI_KEY,
});
const openai = new OpenAIApi(configuration);

async function getCompletion(prompt) {
    try {
        const response = await openai.createChatCompletion({

            model: 'gpt-3.5-turbo',
            temperature: 0,
            messages: [{
                role: 'user',
                content: prompt
            }]
        });

        return response.data.choices[0].message.content.trim();
    } catch (error) {
        console.error('Error during OpenAI API call:', error);
        console.error('Full error object:', JSON.stringify(error, null, 2));
        throw error;
    }
}

module.exports = { getCompletion };
