// In a file called 'db.js' (or whatever you want to name it):

const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://hamzaa:admin@trainwelltest.ym8qe9f.mongodb.net/?retryWrites=true&w=majority"; // Replace with your own MongoDB connection string

const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

function connectDB() {
    return new Promise((resolve, reject) => {
        client.connect(err => {
            if (err) {
                console.error('Failed to connect to MongoDB', err);
                reject(err);
            } else {
                console.log('Connected to MongoDB');
                resolve();
            }
        });
    });
}

module.exports = { client, connectDB };

//
