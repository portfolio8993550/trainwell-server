const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const openai = require('./openai');
const {client, connectDB} = require('./db');
const {ObjectId} = require('mongodb');
const fs = require('fs');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const cors = require('cors');

const corsOptions = {
    origin: 'https://main.d3p9romyu0rpr4.amplifyapp.com',
    methods: ["GET", "POST", "OPTIONS"],
    allowedHeaders: ["Content-Type", "Authorization", "Access-Control-Allow-Origin"]
};

// Explicitly handle OPTIONS requests
app.options('*', (req, res) => {
    res.header("Access-Control-Allow-Origin", "https://main.d3p9romyu0rpr4.amplifyapp.com");
    res.header("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Access-Control-Allow-Origin");
    res.sendStatus(200); // Use sendStatus for sending just a status code
});

app.use(cors(corsOptions));




app.use((req, res, next) => {
    console.log(req.headers);
    next();
});
app.use(bodyParser.json());

app.post('/updateProfile', verifyToken, async (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async (err, authData) => {
        if (err) {
            return res.status(403).json({success: false, message: 'Token is invalid or expired.'});
        }

        if (!authData.email) {
            return res.status(400).json({ success: false, message: 'Email not provided in the token.' });
        }

        const { email } = authData;
        const { profileData } = req.body;

        try {
            const database = client.db('usersDB');
            const users = database.collection('users');

            const updateFields = {};

            // List the fields you want to update
            const fieldsToUpdate = ['age', 'height', 'weight', 'bodyFat', 'activityLevel', 'fitnessGoal', 'fitnessLevel', 'trainingDuration', 'selectedValues'];

            fieldsToUpdate.forEach(field => {
                if (profileData[field] !== undefined) {
                    updateFields[`profileData.${field}`] = profileData[field];
                }
            });

            await users.updateOne({email}, {$set: updateFields});
            res.status(200).json({success: true});
        } catch (error) {
            console.error('Error during profile update:', error);
            res.status(500).json({success: false, message: 'Internal Server Error'});
        }
    });
});



function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'] || req.headers['Authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    } else {
        console.log('Headers without authorization:', req.headers);
        res.status(403).json({ success: false, message: 'No token provided.' });
    }
}

app.get('/getProfileData', verifyToken, async (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async (err, authData) => {
        if (err) {
            res.status(403).json({ success: false, message: 'Token is invalid or expired.' });
            return;  // make sure you exit out of the function
        }

        if (!authData || !authData || !authData.email) {
            console.error("Expected properties are missing from JWT:", authData);
            res.status(400).json({ success: false, message: 'User data not found in token.' });
            return;  // again, make sure you exit out of the function
        }

        const {email} = authData;

        try {
            const database = client.db('usersDB');
            const users = database.collection('users');
            const user = await users.findOne({ email });
            if (user) {
                res.status(200).json({ success: true, profileData: user.profileData });
            } else {
                res.status(404).json({ success: false, message: 'User not found.' });
            }
        } catch (error) {
            console.error('Error during profile fetch:', error);
            res.status(500).json({ success: false, message: 'Internal Server Error' });
        }
    });
});


app.get('/currentUser', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async (err, authData) => {
        if (err) {
            res.status(403).json({ success: false, message: 'Token is invalid or expired.' });
            return;  // make sure you exit out of the function
        }

        if (!authData || !authData.email) {
            console.error("Expected properties are missing from JWT:", authData);
            res.status(400).json({ success: false, message: 'User data not found in token.' });
            return;  // again, make sure you exit out of the function
        }

        try {
            const database = client.db('usersDB');
            const users = database.collection('users');
            const user = await users.findOne({ email: authData.email });

            if (user) {
                // If you don't want to send certain fields, you can delete them from the user object before sending
                // For example: delete user.password;
                res.status(200).json({ success: true, user });
            } else {
                res.status(404).json({ success: false, message: 'User not found.' });
            }
        } catch (error) {
            console.error('Error fetching user:', error);
            res.status(500).json({ success: false, message: 'Internal Server Error' });
        }
    });
});



app.use((req, res, next) => {
    if (req.url.endsWith('.js')) {
        res.type('text/javascript');
    }
    next();
});


app.post('/loginCheck', async (req, res) => {
    const {email, password} = req.body;

    try {
        const database = client.db('usersDB');
        const users = database.collection('users');
        const user = await users.findOne({email});

        if (!user) {
            return res.status(401).json({success: false, message: 'Invalid email.'});
        }

        // check the password
        const isPasswordCorrect = bcrypt.compareSync(password, user.password);

        if (!isPasswordCorrect) {
            return res.status(401).json({success: false, message: 'Invalid password.'});
        }

        // Generate a JWT for the user and send it back
        const userForToken = {
            email: user.email,
            id: user._id  // Using MongoDB's unique _id for the user
        };

        const token = jwt.sign(userForToken, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '1h'});  // The token expires in 1 hour. You can adjust this as necessary.
        res.status(200).json({success: true, token});

    } catch (error) {
        console.error('Error during login:', error);
        res.status(500).json({success: false, message: 'Internal Server Error'});
    }
});
// /logout needs an update
app.get('/logout', (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            // handle error
        }
        res.redirect('/login');
    });
});
// modify the '/signup' post route to include password hashing
app.post('/signup', async (req, res) => {
    const { email, password, profileData } = req.body;

    // encrypt the password
    const hashedPassword = bcrypt.hashSync(password, saltRounds);

    try {
        const database = client.db('usersDB');
        const users = database.collection('users');

        // Check if a user with the same email already exists
        const existingUser = await users.findOne({ email });
        if (existingUser) {
            return res.status(409).json({ success: false, message: 'Email already in use.' });
        }

        // If not, then add the new user with the hashed password and profileData
        const newUser = { email, password: hashedPassword, profileData };
        const result = await users.insertOne(newUser);
        res.status(200).json({ success: true });
    } catch (error) {
        console.error('Error during signup:', error);
        res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
});

app.post('/save-schedule', async (req, res) => {
    const {userId, schedule} = req.body;
    const currentDate = new Date();
    // Insert the new schedule into the Schedule collection
    const database = client.db('usersDB');

    // Retrieve the user's profile information
    const user = await database.collection('users').findOne({_id: new ObjectId(userId)});

    if (!user) {
        res.status(404).send("Failed to find user");
        return;
    }

    const fitnessGoal = user.profileData.fitnessGoal;

    const result = await database.collection('schedules').insertOne({
        userId,
        schedule,
        dateCreated: currentDate,
        fitnessGoal
    });

    if (result.acknowledged) {
        res.status(200).send("Schedule saved successfully");
    } else {
        res.status(500).send("Failed to save schedule");
    }
});

app.post('/getSchedules', async (req, res) => {
    // get the user id from the request body
    const {userId} = req.body;

    // retrieve schedules for this user
    const database = client.db('usersDB');
    const userSchedules = await database.collection('schedules').find({userId}).toArray();

    if (userSchedules.length === 0) {
        res.status(404).send("No schedules found for this user.");
    } else {
        res.status(200).json(userSchedules);
    }
});


app.post('/find-complexity', async (req, res) => {
    const prompt = req.body.prompt;
    const userId = req.body.userId

    if (!prompt) {
        return res.status(400).json({ success: false, message: 'Prompt not provided.' });
    }

    console.log(prompt);
    return res.status(200).json({
        success: true,
        prompt, userId
    });
});
// app.post('/editScheduleName', async (req, res) => {
//
//
// })


const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server listening on port ${port}`));